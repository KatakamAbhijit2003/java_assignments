package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userID;
	@Column(length=30,nullable=false)
	private String firstName;
	@Column(length=30,nullable=false)
	private String lastName;
	private int age;
	@Column(length=30,nullable=false)
	private String gender;
	@Column(length=50,nullable=false)
	private String email;
	@Column(length=10,unique=true)
	private String adminID;
	@Column(length=10,unique=true)
	private String managerID;
	@Column(length=40,nullable=false)
	private String password;
	@Column(length=12,nullable=false)
	private String mobile;
	@Embedded
	private Address address;
}
