package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Hangar {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	private int hangarNo;
	@Column(length=15)
	private String hangarType;
	@Column(length=15,unique=true)
	private String hangarID;
	private int status;
}
