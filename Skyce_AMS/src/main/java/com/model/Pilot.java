package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@SuppressWarnings("serial")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Pilot implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pilotNo;
	@Column(length=30)
	private String firstName;
	@Column(length=30)
	private String lastName;
	private int age;
	@Column(length=45,unique=true)
	private String email;
	@Column(length=10,unique=true,nullable=false)
	private String dgcaPilotID;
}
