package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Address implements Serializable{
	@Column(length=15,nullable=false)
	private String PIN;
	@Column(length=30,nullable=false)
	private String city;
	@Column(length=50,nullable=false)
	private String state;
	@Column(length=30,nullable=false)
	private String country;
	@Column(length=30,nullable=false)
	private String street;
	@Column(length=15,nullable=false)
	private String doorNo;
}
