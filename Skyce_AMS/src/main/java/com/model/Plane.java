package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Plane implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int planeNo;
	@Column(length=15,unique=true,nullable=false)
	private String planeID;
	@Column(length=40)
	private String modelName;
	@Column(length=30)
	private String planeCompany;
	private int planeCapacity;
	@Column(length=30)
	private String planeType;
	@Column(length=15,nullable=true)
	private String hangarID;
}
