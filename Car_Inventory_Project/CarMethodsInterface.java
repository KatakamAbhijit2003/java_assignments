package cardealership;

import java.util.List;

public interface CarMethodsInterface {
	boolean carUpdateMake(String carID,String make);
	boolean carDelete(Car car);
	boolean carUpdateModel(String carID,String model);
	boolean carUpdateYear(String carID,int year);
	boolean carUpdatePrice(String carID,double price);
	boolean carAdd(Car car);
	List<Car> carDisplay();
	int carCount();

}
