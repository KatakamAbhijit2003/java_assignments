SQL Assignment

Create Table
1.	Write a SQL statement to create a simple table countries including columns country_id,country_name and region_id
Solution:
MySQL>create table countries(country_id varchar(2), country_name varchar(30), region_id varchar(2));
Query OK, 0 rows affected (0.50 sec)

2.	Write a SQL statement to create a table countries set a constraint NULL
Solution:
MySQL>create table countries(country_id varchar(2) not null,country_name varchar(30), region_id varchar(2));
Query OK, 0 rows affected (0.06 sec)

3.	Create table locations including columns. 
LOCATION_ID         	decimal(4,0) 
STREET_ADDRESS  	varchar(40)
POSTAL_CODE       	varchar(12)
CITY   	                             varchar(30) 
STATE_PROVINCE 	varchar(25) 
COUNTRY_ID         	varchar(2)
Solution:
MySQL>create table locations(location_id decimal(4,0),street_address varchar(40),postal_code varchar(12),city varchar(30), state_province varchar(25),country_id varchar(2));
Query OK, 0 rows affected (0.10 sec)

Alter Table
1.	Write a SQL statement to rename the table countries to country_new.
Solution:
MySQL>alter table countries rename to country_new;
Query OK, 0 rows affected (0.36 sec)

2.	Write a SQL statement to add a columns ID as the first column of the table locations
Solution:
MySQL>alter table locations add column ID varchar(5) first;
Query OK, 0 rows affected (0.13 sec)
Records: 0  Duplicates: 0  Warnings: 0

3.	Write a SQL statement to add a column region_id after state_province to the table locations
Solution:
MySQL>alter table locations add column region_id varchar(2) after state_province;
Query OK, 0 rows affected (0.15 sec)
Records: 0  Duplicates: 0  Warnings: 0

4.	Write a SQL statement change the data type of the column country_id to integer in the table locations.
Solution:
MySQL>alter table locations modify country_id int;
Query OK, 0 rows affected (0.18 sec)
Records: 0  Duplicates: 0  Warnings: 0

Insert table
1.	Write a SQL statement to insert 3 rows by a single insert statement
Solution:
MySQL>insert into countries values ('IN','India','DL'),('RS','Russia','MW'),('CH','China','BJ');
Query OK, 3 rows affected (0.02 sec)
Records: 3  Duplicates: 0  Warnings: 0

2.	Write a SQL statement to insert rows into the table countries in which the value of country_id column will be unique and auto incremented
Solution:
First we’ll convert the column country_id to int type and set to auto increment and add a unique constraint.
2.1.	MySQL>alter table countries modify country_id int auto_increment;
Query OK, 0 rows affected (0.15 sec)
Records: 0  Duplicates: 0  Warnings: 
2.2.	MySQL>alter table countries add constraint unique key(country_id);
Query OK, 0 rows affected (0.27 sec)
Records: 0  Duplicates: 0  Warnings: 0
MySQL>insert into countries(country_name,region_id) values ('Italy','RM'),('France','PS');
Query OK, 2 rows affected (0.02 sec)
Records: 2  Duplicates: 0  Warnings: 0
MySQL>select * from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | Italy        | RM        |
|          2 | France       | PS        |
+------------+--------------+-----------+
2 rows in set (0.00 sec)

3.	Write a SQL statement to insert rows only for country_id and country_name.
Solution:
MySQL>insert into countries(country_id,country_name) values (3,'India');
Query OK, 1 row affected (0.01 sec)

MySQL>select * from countries;
+------------+--------------+-----------+
| country_id | country_name | region_id |
+------------+--------------+-----------+
|          1 | Italy        | RM        |
|          2 | France       | PS        |
|          3 | India        | NULL      |
+------------+--------------+-----------+
3 rows in set (0.00 sec)
Update Table
The employee table has been created and the rows have been inserted.
MySQL>select * from employees;
+-------------+------------+----------+--------+---------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL    | SALARY | COMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+----------+--------+---------------+---------------+
|         100 | Steven     | SKING    |  24000 |             0 |            90 |
|         101 | Neena      | NKOCHHAR |   1700 |             0 |            90 |
|         102 | Lex        | LDEHAAN  |  17000 |             0 |            90 |
|         103 | ALexander  | AHUNOLD  |   9000 |             0 |            60 |
|         104 | Bruce      | Bernst   |   6000 |             0 |            60 |
|         105 | David      | DAUSTIN  |   4800 |             0 |            60 |
|         106 | Valli      | VPATABAL |   4200 |             0 |            60 |
|         107 | Diana      | DLORENTZ |  12008 |             0 |           110 |
|         205 | Shelley    | SHIGGINS |   8300 |             0 |           110 |
|         206 | William    | WGIETZ   |   8300 |             0 |           110 |
+-------------+------------+----------+--------+---------------+---------------+
10 rows in set (0.00 sec)
1.	Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for all employees
Solution:
MySQL>update employees set EMAIL='not available',comission_pct=0.00;
Query OK, 10 rows affected (0.01 sec)
Rows matched: 10  Changed: 10  Warnings: 0

MySQL>select * from employees;
+-------------+------------+---------------+----------+---------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY   | COMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+----------+---------------+---------------+
|         100 | Steven     | not available | 24000.00 |          0.10 |            90 |
|         101 | Neena      | not available |  1700.00 |          0.10 |            90 |
|         102 | Lex        | not available | 17000.00 |          0.10 |            90 |
|         103 | ALexander  | not available |  9000.00 |          0.10 |            60 |
|         104 | Bruce      | not available |  6000.00 |          0.10 |            60 |
|         105 | David      | not available |  4800.00 |          0.10 |            60 |
|         106 | Valli      | not available |  4200.00 |          0.10 |            60 |
|         107 | Diana      | not available | 12008.00 |          0.10 |           110 |
|         205 | Shelley    | not available |  8300.00 |          0.10 |           110 |
|         206 | William    | not available |  8300.00 |          0.10 |           110 |
+-------------+------------+---------------+----------+---------------+---------------+
10 rows in set (0.00 sec)
2.	Write a SQL statement to change the email and commission_pct column of employees table with 'not available' and 0.10 for those employees whose department_id is 110.
Solution:
MySQL>update employees set email='not available', comission_pct=0.10 where department_id=110;
Query OK, 3 rows affected (0.01 sec)
Rows matched: 3  Changed: 3  Warnings: 0
3.	Write a SQL statement to change salary of employee to 8000 whose ID is 105, if the existing salary is less than 5000.
Solution:
MySQL>update employees set salary=8000 where employee_id=105 and salary<5000;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MySQL>select * from employees;
+-------------+------------+---------------+----------+---------------+---------------+
| EMPLOYEE_ID | FIRST_NAME | EMAIL         | SALARY   | COMISSION_PCT | DEPARTMENT_ID |
+-------------+------------+---------------+----------+---------------+---------------+
|         100 | Steven     | not available | 24000.00 |          0.00 |            90 |
|         101 | Neena      | not available |  1700.00 |          0.00 |            90 |
|         102 | Lex        | not available | 17000.00 |          0.00 |            90 |
|         103 | ALexander  | not available |  9000.00 |          0.00 |            60 |
|         104 | Bruce      | not available |  6000.00 |          0.00 |            60 |
|         105 | David      | not available |  8000.00 |          0.00 |            60 |
|         106 | Valli      | not available |  4200.00 |          0.00 |            60 |
|         107 | Diana      | not available | 12008.00 |          0.10 |           110 |
|         205 | Shelley    | not available |  8300.00 |          0.10 |           110 |
|         206 | William    | not available |  8300.00 |          0.10 |           110 |
+-------------+------------+---------------+----------+---------------+---------------+
10 rows in set (0.00 sec)

Select statement questions
1.	Write a SQL statement to display all the information of all salesmen
Solution:
MySQL>select * from salesman;
+-------------+------------+----------+------------+
| salesman_id | name       | city     | commission |
+-------------+------------+----------+------------+
|        5001 | James Hoog | New York |       0.15 |
|        5002 | Nail Knite | Paris    |       0.13 |
|        5005 | Pit Alex   | London   |       0.11 |
|        5006 | Mc Lyon    | Paris    |       0.14 |
|        5003 | Lauson Hen | NULL     |       0.12 |
|        5007 | Paul Adam  | Rome     |       0.13 |
+-------------+------------+----------+------------+
6 rows in set (0.00 sec)

2.	Write a SQL statement to display specific columns like name and commission for all the salesmen
Solution:
MySQL>select name,commission from salesman;
+------------+------------+
| name       | commission |
+------------+------------+
| James Hoog |       0.15 |
| Nail Knite |       0.13 |
| Pit Alex   |       0.11 |
| Mc Lyon    |       0.14 |
| Lauson Hen |       0.12 |
| Paul Adam  |       0.13 |
+------------+------------+
6 rows in set (0.00 sec)

3.	Write a SQL statement to display names and city of salesman, who belongs to the city of Paris
Solution:
MySQL>select name,city from salesman where city='Paris';
+------------+-------+
| name       | city  |
+------------+-------+
| Nail Knite | Paris |
| Mc Lyon    | Paris |
+------------+-------+
2 rows in set (0.00 sec)

4.	Write a query to display the columns in a specific order like order date, salesman id, order number and purchase amount from for all the orders
Solution:
MySQL>select ord_date,salesman_id,ord_no,purch_amt from orders;
+------------+-------------+--------+-----------+
| ord_date   | salesman_id | ord_no | purch_amt |
+------------+-------------+--------+-----------+
| 05-10-2012 |        5002 |  70001 |    150.50 |
| 05-10-2012 |        5001 |  70002 |     65.26 |
| 10-10-2012 |        5003 |  70003 |   2480.40 |
| 17-08-2012 |        5003 |  70004 |    110.50 |
| 27-07-2012 |        5001 |  70005 |   2400.60 |
| 10-09-2012 |        5002 |  70007 |    948.50 |
| 10-09-2012 |        5001 |  70008 |   5760.00 |
| 10-09-2012 |        5005 |  70009 |    270.65 |
| 10-10-2012 |        5006 |  70010 |   1983.43 |
| 17-08-2012 |        5007 |  70011 |     75.29 |
| 27-06-2012 |        5002 |  70012 |    250.45 |
| 25-04-2012 |        5001 |  70013 |   3045.60 |
+------------+-------------+--------+-----------+
12 rows in set (0.00 sec)

5.	Write a query which will retrieve the value of salesman id of all salesmen, getting orders from the customers in orders table without any repeats
Solution:
MySQL>select distinct salesman_id from orders;
+-------------+
| salesman_id |
+-------------+
|        5002 |
|        5001 |
|        5003 |
|        5005 |
|        5006 |
|        5007 |
+-------------+
6 rows in set (0.01 sec)

6.	Write a SQL statement to display all the information for those customers with a grade of 200.
Solution:
MySQL>select * from customers where grade=200;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3003 | Jozy Altidor | Moscow     |   200 |        5007 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
+-------------+--------------+------------+-------+-------------+
3 rows in set (0.00 sec)

7.	Write a SQL query to find all the products with a price between Rs.200 and Rs.600.
Solution:
MySQL>select * from products where PRO_PRICE between 200 and 600;
+--------+------------------+-----------+---------+
| PRO_ID | PRO_NAME         | PRO_PRICE | PRO_COM |
+--------+------------------+-----------+---------+
|    102 | Key Board        |       450 |      16 |
|    103 | ZIP drive        |       250 |      14 |
|    104 | Speaker          |       550 |      16 |
|    109 | Refill Cartridge |       350 |      13 |
|    110 | Mouse            |       250 |      12 |
+--------+------------------+-----------+---------+
5 rows in set (0.00 sec)

Aggregation Function
1.	Write a SQL statement to find the total purchase amount of all orders.
Solution:
MySQL>select sum(purch_amt) from orders;
+----------------+
| sum(purch_amt) |
+----------------+
|       17541.18 |
+----------------+
1 row in set (0.01 sec)

2.	Write a SQL statement to find the average purchase amount of all orders.
Solution:
MySQL>select avg(purch_amt) from orders;
+----------------+
| avg(purch_amt) |
+----------------+
|    1461.765000 |
+----------------+
1 row in set (0.01 sec)

3.	Write a SQL statement to find the number of salesmen currently listing for all of their customers
Solution:
MySQL>select count(distinct(salesman_id)) from customers;
+------------------------------+
| count(distinct(salesman_id)) |
+------------------------------+
|                            6 |
+------------------------------+
1 row in set (0.01 sec)

4.	Write a SQL statement know how many customer have listed their names.
Solution:
MySQL>select count(customer_id) from customers where cust_name is not null;
+--------------------+
| count(customer_id) |
+--------------------+
|                  8 |
+--------------------+
1 row in set (0.00 sec)

5.	Write a SQL statement to get the maximum purchase amount of all the orders
Solution:
MySQL>select max(purch_amt) from orders;
+----------------+
| max(purch_amt) |
+----------------+
|        5760.00 |
+----------------+
1 row in set (0.00 sec)

Relational Operator
1.	Write a query to display all customers with a grade above 100
Solution: 
MySQL>select * from customers where grade>100;
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3003 | Jozy Altidor | Moscow     |   200 |        5007 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3008 | Julian Green | London     |   300 |        5002 |
+-------------+--------------+------------+-------+-------------+
5 rows in set (0.00 sec)

2.	Write a query statement to display all customers in New York who have a grade value above 100
Solution: 
MySQL>select * from customers where grade>100 and city='New York';
+-------------+------------+----------+-------+-------------+
| customer_id | cust_name  | city     | grade | salesman_id |
+-------------+------------+----------+-------+-------------+
|        3007 | Brad Davis | New York |   200 |        5001 |
+-------------+------------+----------+-------+-------------+
1 row in set (0.00 sec)

3.	Write a SQL statement to display all customers, who are either belongs to the city New York or had a grade above 100
Solution:
MySQL>select * from customers where grade>100 or city='New York';
+-------------+--------------+------------+-------+-------------+
| customer_id | cust_name    | city       | grade | salesman_id |
+-------------+--------------+------------+-------+-------------+
|        3002 | Nick Rimando | New York   |   100 |        5001 |
|        3003 | Jozy Altidor | Moscow     |   200 |        5007 |
|        3004 | Fabian Johns | Paris      |   300 |        5006 |
|        3005 | Graham Zusi  | California |   200 |        5002 |
|        3007 | Brad Davis   | New York   |   200 |        5001 |
|        3008 | Julian Green | London     |   300 |        5002 |
+-------------+--------------+------------+-------+-------------+
6 rows in set (0.00 sec)

4.	Write a SQL statement to display either those orders which are not issued on date 2012-09-10 and issued by the salesman whose ID is 505 and below or those orders which purchase amount is 1000.00 and below.
Solution:
MySQL>select * from orders where (ord_date<>'10-09-2012' and salesman_id <=5005) or purch_amt <=1000.00;
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70001 |    150.50 | 05-10-2012 |        3005 |        5002 |
|  70002 |     65.26 | 05-10-2012 |        3002 |        5001 |
|  70003 |   2480.40 | 10-10-2012 |        3009 |        5003 |
|  70004 |    110.50 | 17-08-2012 |        3009 |        5003 |
|  70005 |   2400.60 | 27-07-2012 |        3007 |        5001 |
|  70007 |    948.50 | 10-09-2012 |        3005 |        5002 |
|  70009 |    270.65 | 10-09-2012 |        3001 |        5005 |
|  70011 |     75.29 | 17-08-2012 |        3003 |        5007 |
|  70012 |    250.45 | 27-06-2012 |        3008 |        5002 |
|  70013 |   3045.60 | 25-04-2012 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
10 rows in set (0.00 sec)

Sorting and filtering
1.	Write a query in SQL to display the full name (first and last name), and salary for those employees who earn below 6000
Solution:
MySQL>select concat(first_name,' ',last_name) from employees where salary<6000;
+----------------------------------+
| concat(first_name,' ',last_name) |
+----------------------------------+
| Neena Kochhar                    |
| Valli Patabal                    |
+----------------------------------+
2 rows in set (0.00 sec)
2.	Write a query in SQL to display the first and last name, and department number for all employees whose last name is "Ernst".
Solution:
MySQL>select concat(first_name,' ',last_name) as 'Full Name',department_id from employees where last_name='Ernst';
+-------------+---------------+
| Full Name   | department_id |
+-------------+---------------+
| Bruce Ernst |            60 |
+-------------+---------------+
1 row in set (0.00 sec)

3.	Write a query in SQL to display the full name (first and last),  salary, and department number for those employees whose first name does not containing the letter M and make the result set in ascending order by department number.
Solution:
MySQL>select concat(first_name,' ',last_name)full_name,salary,department_id from employees where first_name not like 'M%' order by department_id;
+------------------+----------+---------------+
| full_name        | salary   | department_id |
+------------------+----------+---------------+
| ALexander Hunold |  9000.00 |            60 |
| Bruce Ernst      |  6000.00 |            60 |
| David Austin     |  8000.00 |            60 |
| Valli Patabal    |  4200.00 |            60 |
| Steven King      | 24000.00 |            90 |
| Neena Kochhar    |  1700.00 |            90 |
| Lex Deehan       | 17000.00 |            90 |
| Diana Lorentz    | 12008.00 |           110 |
| Shelley Higgins  |  8300.00 |           110 |
| William Geitz    |  8300.00 |           110 |
+------------------+----------+---------------+
10 rows in set (0.01 sec)

4.	Write a query in SQL to display the full name (first and last name), and salary for all employees who does not earn any commission
Solution:
MySQL>select concat(first_name,' ',last_name)full_name, salary from employees where comission_pct=0;
+------------------+----------+
| full_name        | salary   |
+------------------+----------+
| Steven King      | 24000.00 |
| Neena Kochhar    |  1700.00 |
| Lex Deehan       | 17000.00 |
| ALexander Hunold |  9000.00 |
| Bruce Ernst      |  6000.00 |
| David Austin     |  8000.00 |
| Valli Patabal    |  4200.00 |
+------------------+----------+
7 rows in set (0.00 sec)

Subqueries
1.	Write a query to display all the orders from the orders table issued by the salesman 'Paul Adam'.
Solution:
MySQL>select * from orders where salesman_id=(select salesman_id from salesman where name='Paul Adam');
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70011 |     75.29 | 17-08-2012 |        3003 |        5007 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)

2.	Write a query to display all the orders for the salesman who belongs to the city London.
Solution:
MySQL>select * from orders where salesman_id=(select salesman_id from salesman where city='London');
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70009 |    270.65 | 10-09-2012 |        3001 |        5005 |
+--------+-----------+------------+-------------+-------------+
1 row in set (0.00 sec)
3.	Write a query to find all the orders issued against the salesman who works for customer whose id is 3007.
Solution:
MySQL>select * from orders where salesman_id in(select salesman_id from salesman where salesman_id in(select salesman_id from customers where customer_id=3007));
+--------+-----------+------------+-------------+-------------+
| ord_no | purch_amt | ord_date   | customer_id | salesman_id |
+--------+-----------+------------+-------------+-------------+
|  70002 |     65.26 | 05-10-2012 |        3002 |        5001 |
|  70005 |   2400.60 | 27-07-2012 |        3007 |        5001 |
|  70008 |   5760.00 | 10-09-2012 |        3002 |        5001 |
|  70013 |   3045.60 | 25-04-2012 |        3002 |        5001 |
+--------+-----------+------------+-------------+-------------+
4 rows in set (0.01 sec)

4.	Write a query to display the commission of all the salesmen servicing customers in Paris.
Solution:
MySQL>select commission from salesman where salesman_id in (select salesman_id from customers where city='Paris');
+------------+
| commission |
+------------+
|       0.14 |
+------------+
1 row in set (0.00 sec)

Joins
1.	Write a query in SQL to display the first name, last name, department number, and department name for each employee
Solution:
mysql> select e.first_name,e.last_name,d.department_id,d.department_name from employees e join departments d using(department_id);
+------------+-----------+---------------+-----------------+
| first_name | last_name | department_id | department_name |
+------------+-----------+---------------+-----------------+
| ALexander  | Hunold    |            60 | IT              |
| Bruce      | Ernst     |            60 | IT              |
| David      | Austin    |            60 | IT              |
| Valli      | Patabal   |            60 | IT              |
| Steven     | King      |            90 | Executive       |
| Neena      | Kochhar   |            90 | Executive       |
| Lex        | Dehaan    |            90 | Executive       |
| Diana      | Lorentz   |           110 | Accounting      |
| Shelley    | Higgins   |           110 | Accounting      |
| William    | Geitz     |           110 | Accounting      |
+------------+-----------+---------------+-----------------+
10 rows in set (0.01 sec)

2.	Write a query in SQL to display the first name, last name, department number and department name, for all employees for departments 80 or 40
Solution:
MySQL> select e.first_name,e.last_name,d.department_name,d.department_id from employees e,departments d where e.department_id=d.department_id and (d.department_id=40 or d.department_id=80);
Empty set (0.00 sec)
3.	Write a query in SQL to display the first name of all employees including the first name of their manager.
Solution:
MySQL> select e.first_name as "Employee_name",m.first_name as "Manager_name" from employees e join departments d on (e.department_id=d.department_id) join employees m on(m.employee_id=d.manager_id);
+---------------+--------------+
| Employee_name | Manager_name |
+---------------+--------------+
| Steven        | Steven       |
| Neena         | Steven       |
| Lex           | Steven       |
| ALexander     | ALexander    |
| Bruce         | ALexander    |
| David         | ALexander    |
| Valli         | ALexander    |
| Diana         | Shelley      |
| Shelley       | Shelley      |
| William       | Shelley      |
+---------------+--------------+
10 rows in set (0.00 sec)
4.	Write a query in SQL to display all departments including those where does not have any employee
Solution:
MySQL> select distinct d.department_name from departments d left outer join employees e using (department_id);
+------------------+
| department_name  |
+------------------+
| Administration   |
| Marketing        |
| Purchasing       |
| Human Resources  |
| Shipping         |
| IT               |
| Public Relations |
| Sales            |
| Executive        |
| Finance          |
| Accounting       |
+------------------+
11 rows in set (0.00 sec)

5.	Write a query in SQL to display the first name, last name, department number and name, for all employees who have or have not any department
Solution:
MySQL> select e.first_name,e.last_name,d.department_name,d.department_id from employees e left outer join departments d using (department_id);
+------------+-----------+-----------------+---------------+
| first_name | last_name | department_name | department_id |
+------------+-----------+-----------------+---------------+
| Steven     | King      | Executive       |            90 |
| Neena      | Kochhar   | Executive       |            90 |
| Lex        | Dehaan    | Executive       |            90 |
| ALexander  | Hunold    | IT              |            60 |
| Bruce      | Ernst     | IT              |            60 |
| David      | Austin    | IT              |            60 |
| Valli      | Patabal   | IT              |            60 |
| Diana      | Lorentz   | Accounting      |           110 |
| Shelley    | Higgins   | Accounting      |           110 |
| William    | Geitz     | Accounting      |           110 |
+------------+-----------+-----------------+---------------+
10 rows in set (0.00 sec)

